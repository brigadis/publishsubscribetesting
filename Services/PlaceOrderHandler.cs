﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using NServiceBus;

namespace Services
{
    class PlaceOrderHandler : IHandleMessages<PlaceOrder>
    {
        public void Handle(PlaceOrder message)
        {
            //throw new Exception("test");
            Console.WriteLine("order " + message.OrderId + ", " + message.Message);
        }
    }
}
