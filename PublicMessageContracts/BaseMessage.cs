﻿using System;

namespace PublicMessageContracts
{
    public class BaseMessage
    {
        public string Message { get; set; }
        public DateTime SentTime { get; set; }
        public bool IsMessageSupposedToBeFunny { get; set; }
        public int MessageIQ { get; set; } 
    }
}