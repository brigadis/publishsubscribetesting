﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Contracts;
using NServiceBus;
using StructureMap;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private readonly IBus _buz;

        public HomeController(IBus buz)
        {
            if (buz == null) throw new ArgumentNullException("buz");
            _buz = buz;
        }


        public void Index()
        {
            var orderId = Guid.NewGuid();
            _buz.Send(new PlaceOrder {OrderId = orderId, Message = "Får du det til?"});
        }
    }
}
