﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;

namespace Contracts
{
    public class PlaceOrder : IMessage
    {
        public Guid OrderId { get; set; }
        public string Message { get; set; }
    }
}
